%% File to run all tests

%% dual run test

resultDualRun = runtests('dualRunTest');

%% full test
resultFullTest = runtests('fullTest');

result = [resultDualRun, resultFullTest];

%% Make a nice table and catch error messages

clear msg
T = table(result);
for idx = 1:height(T)
    if T.Failed(idx)
        msg{idx} = result(idx).Details.DiagnosticRecord.Exception.message;
    else
        msg{idx} = 'all good';
    end
end
T.msg = msg';

%% Show results

fprintf('\n\n%d out of %d tests passed: \n\n', sum(T.Passed == true), height(T))

disp(T(:, [1:2 end]))