% Test the output when fitting the Ladder Network Model (LNM) only twice on an EFI
% measured from a participant from the CBU
%
% The start values are fixed (LNM.jitter_start_values_percent = 0;),
% the output values should therefore always be the same when ran twice.
% The actual values might differ however when ran on different machines.
%
% So the main focus here it a basic check that the code runs smoothly.

% Save output
[current_path, ~, ~] = fileparts(mfilename('fullpath'));
output_folder = [current_path filesep 'dualRunTest'];

% if test already run, load raw data
if exist(output_folder, 'dir')
    disp('Test already done, reloading raw data. Run "clearAllTests" to clear output.')
    load([output_folder filesep 'dualRunTest.mat'])
else
    % EFI from CBU participant AB24, measured on 20/05/2023
    EFI_ohms = [...
        4387        1106        1017         945         927         910         874         803         803         749         731         696         660         642         624         589
        1106        4494        1106         963         927         874         856         803         767         749         713         696         642         642         624         589
        1017        1106        4708        1052         945         910         856         803         785         731         713         696         660         642         624         589
        944         963        1070        4815        1052         945         874         820         803         767         731         696         660         642         624         589
        926         927         945        1070        4494        1070         945         874         945         785         749         731         749         642         642         589
        890         892         892         945        1070        5457        1070         927         874         803         767         731         713         660         642         606
        855         856         856         874         945        1070        5778        1052         945         856         803         767         749         713         642         624
        820         803         803         838         874         927        1070        4387        1106         945         874         803         767         749         696         642
        784         767         767         803         945         856         945        1159        8025        1213         945         856         874         767         749         642
        748         731         749         767         785         803         856         927        1213        5778        1070         927         856         803         767         696
        730         713         713         731         749         767         803         856         945        1070        5136        1034         945         874         803         731
        695         696         678         696         713         731         767         803         874         910        1017        5457        1106         927         856         767
        677         642         642         642         749         713         731         767         874         874         945        1106        6420        1106         927         803
        641         642         642         642         642         660         713         731         767         803         856         945        1106        6099        1070         856
        606         606         606         624         624         624         642         642         731         749         803         820         910        1070       18297        1195
        570         571         553         571         589         589         606         624         642         642         696         731         767         820        1195       19795];

    % Create object and load EFI
    LNM = LadderNetworkModel;
    LNM.set_Z(EFI_ohms);

    % set properties
    LNM.order = 15;
    LNM.jitter_start_values_percent = 0;
    LNM.min_max_transversal_resistors_ohms = [1e2 1e8];
    LNM.min_max_longitudinal_resistors_ohms = [1 1e12];
    LNM.max_iterations_per_fit_per_parameter = 200;
    LNM.max_transversal_excursion_octave = 1;
    LNM.max_longitudinal_excursion_octave = 0.2;
    LNM.max_transversal_current_excursion_percent = 15;
    LNM.max_excursion_layers_octave = 1;
    LNM.nonlinear_constraints = 1;
    LNM.error_scaling = 1e6;
    LNM.number_fits = 2; % this would usually be set to 200, but here we check that it is the same when ran twice

    tic
    LNM.fit_model
    toc

    mkdir(output_folder)

    plot(LNM)

    set(gcf, 'PaperUnits', 'centimeters')
    set(gcf, 'PaperSize', [20 15])
    set(gcf, 'PaperPosition', [0 0 20 15])
    saveas(gcf, [output_folder filesep 'dualRunTest.pdf'])

    save([output_folder filesep 'dualRunTest.mat'], 'LNM', 'EFI_ohms')
end

%% Status is correct
% Status should be 2 if fitted

tol = 0;
expected_value = 2;
value = LNM.status;
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)

%% Output dimensions are correct
% E.g. as many modelled Zn as runs

tol = 0;
expected_value = 2;
value = size(LNM.modelled_Zn_ohms, 3);
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)

%% Outputs of both runs are identical

tol = 0;
expected_value = 0;
value = abs(LNM.modelled_Zn_ohms(:, :, 2) - ...
    LNM.modelled_Zn_ohms(:, :, 1));
assert(abs(sum(value(:)) - expected_value) <= tol,...
    'Average resistor difference between runs is %d. Expected %d', ...
    abs(sum(value(:))), expected_value)
