%% File to clear output of all tests

%% dual run test

current_folder = fileparts(mfilename('fullpath'));
analysis_folder = [current_folder filesep 'dualRunTest'];

if exist(analysis_folder, 'dir')
    rmdir(analysis_folder, 's')
end
clear analysis_folder current_folder

%% full test

current_folder = fileparts(mfilename('fullpath'));
analysis_folder = [current_folder filesep 'fullTest'];

if exist(analysis_folder, 'dir')
    rmdir(analysis_folder, 's')
end
clear analysis_folder current_folder
