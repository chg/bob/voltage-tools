% Test the output when fitting the Ladder Network Model (LNM) 200 times on an EFI
% created from a known set of resistors
%
% The actual output for each run might differ depending on the machine/matlab version,
% but the average solution should be the same (hopefully!).
%

% Save output
[current_path, ~, ~] = fileparts(mfilename('fullpath'));
output_folder = [current_path filesep 'fullTest'];

% if test already run, load raw data
if exist(output_folder, 'dir')
    disp('Test already done, reloading raw data. Run "clearAllTests" to clear output.')
    load([output_folder filesep 'fullTest.mat'])
else
    % R matrix defining the ladder network
    true_R_ohms = [...
        14847         738        4977        9324        8971       13016       28454       38191      126073      152124       80474       96390      119745       77435       96543       88254
        738        9652         484        7534       12536        9438       20733       44490       60007      106763      145853      103716      128973       83093       75980       62700
        4977         484        9426         708       11207       12533       14228       33514       69947       45801       89216      170056      134632      107900       87598       69663
        9324        7534         708        9881         851        7695       18565       17283       51663       81714       36109       91869      224431      144418      132386       68366
        8971       12536       11207         851       25008         854       11591       20430       11124       72756       89524       44890      100427      243739      165472      152119
        13016        9438       12533        7695         854       13762        1141       12281       29509       18718       61651       76483       53063      137726      183350      114431
        28454       20733       14228       18565       11591        1141       20053        1576       17525       30499       25205       47318       74017       70947      162457      125373
        38191       44490       33514       17283       20430       12281        1576       17622        1595       19628       20542       24955       67749       73695      106606      118641
        126073       60007       69947       51663       11124       29509       17525        1595       69176        1334       19164       29686       18534       78330      105992      158976
        152124      106763       45801       81714       72756       18718       30499       19628        1334       20331        1720       12788       23842       25870       56552      117456
        80474      145853       89216       36109       89524       61651       25205       20542       19164        1720       17179        1582        9100       17148       22243       49881
        96390      103716      170056       91869       44890       76483       47318       24955       29686       12788        1582       14202        1197       10080       24832       24469
        119745      128973      134632      224431      100427       53063       74017       67749       18534       23842        9100        1197       17114        1388       13840       29223
        77435       83093      107900      144418      243739      137726       70947       73695       78330       25870       17148       10080        1388       10229        2126       20835
        96543       75980       87598      132386      165472      183350      162457      106606      105992       56552       22243       24832       13840        2126       14696        1750
        88254       62700       69663       68366      152119      114431      125373      118641      158976      117456       49881       24469       29223       20835        1750        6393];

    EFI_ohms = LadderNetworkModel.get_node_impedance_matrix(16, 15, true_R_ohms);
    true_EFI_ohms = EFI_ohms;

    % the diagonal/peak values should have a contact impedance element,
    % here we are taking the values from AB24
    diag_values_AB24 = ...
        [4387 4494 4708 4815 4494 5457 5778 4387 8025 5778 5136 5457 6420 6099 18297 19795];
    EFI_ohms(1:17:end) = diag_values_AB24;


    % Create object and load EFI
    LNM = LadderNetworkModel;
    LNM.set_Z(EFI_ohms);

    % set properties
    LNM.order = 15;
    LNM.jitter_start_values_percent = 5;
    LNM.min_max_transversal_resistors_ohms = [1e2 1e8];
    LNM.min_max_longitudinal_resistors_ohms = [1 1e12];
    LNM.max_iterations_per_fit_per_parameter = 200;
    LNM.max_transversal_excursion_octave = 1;
    LNM.max_longitudinal_excursion_octave = 0.2;
    LNM.max_transversal_current_excursion_percent = 20;
    LNM.max_excursion_layers_octave = 1;
    LNM.nonlinear_constraints = 1;
    LNM.error_scaling = 1e6;
    LNM.number_fits = 200;

    tic
    LNM.fit_model
    toc

    % find best fit, based on closest to median fitted resistors
    median_R_ohms = median(LNM.modelled_R_ohms(:, :, LNM.limits_reached_bool == 0), 3);
    for idx = LNM.number_fits:-1:1
        if ~LNM.limits_reached_bool(idx)
            R_ohms_here = squeeze(LNM.modelled_R_ohms(:, :, idx));
            out_R(idx) =  sum(sum((log10(R_ohms_here) - log10(median_R_ohms)).^2, 'omitnan'), 'omitnan');
        else
            out_R(idx) = NaN;
        end
    end
    [~, idx_best_R] = min(out_R);
    best_R_ohms = squeeze(LNM.modelled_R_ohms(:, :, idx_best_R));
    best_Zn_ohms = squeeze(LNM.modelled_Zn_ohms(:, :, idx_best_R));

    mkdir(output_folder)

    save([output_folder filesep 'fullTest.mat'], 'LNM', 'EFI_ohms', 'true_R_ohms', 'true_EFI_ohms', 'best_R_ohms', 'best_Zn_ohms')

    plot(LNM)

    set(gcf, 'PaperUnits', 'centimeters')
    set(gcf, 'PaperSize', [20 15])
    set(gcf, 'PaperPosition', [0 0 20 15])
    saveas(gcf, [output_folder filesep 'fullTest.pdf'])

    figure
    true_peak_values = true_EFI_ohms(1:17:end);
    best_peak_values = best_Zn_ohms(1:17:end);
    true_off_peak_values = true_EFI_ohms(eye(16) == 0);
    best_off_peak_values = best_Zn_ohms(eye(16) == 0);

    subplot(1, 2, 1)
    hold on
    plot([1 5000], [1 5000], 'k-')
    plot(true_peak_values, best_peak_values, 'x', 'LineWidth', 2)
    ax = gca;
    ax.XScale = 'log';
    ax.YScale = 'log';
    ax.FontSize = 16;
    ax.LineWidth = 2;
    xlabel('True Zn values (Ohms)')
    ylabel('Best fitted Zn values (Ohms)')
    title('Peak values')
    xlim([1200 2200])
    ylim([1200 2200])
    legend('Ideal', 'Model Output', 'Location','southeast')

    subplot(1, 2, 2)
    hold on
    plot([1 5000], [1 5000], 'k-')
    plot(true_off_peak_values, best_off_peak_values, 'x', 'LineWidth', 2)
    ax = gca;
    ax.XScale = 'log';
    ax.YScale = 'log';
    ax.FontSize = 16;
    ax.LineWidth = 2;
    xlabel('True Zn values (Ohms)')
    ylabel('Best fitted Zn values (Ohms)')
    title('Off-peak values')
    xlim([500 1500])
    ylim([500 1500])
    legend('Ideal', 'Model Output', 'Location','southeast')

    set(gcf, 'PaperUnits', 'centimeters')
    set(gcf, 'PaperSize', [25 10])
    set(gcf, 'PaperPosition', [0 0 25 10])
    saveas(gcf, [output_folder filesep 'fullTest_Zn_ohms.pdf'])

    figure
    true_peak_values = true_R_ohms(1:17:end);
    best_peak_values = best_R_ohms(1:17:end);
    true_off_peak_values = true_R_ohms(eye(16) == 0);
    best_off_peak_values = best_R_ohms(eye(16) == 0);

    subplot(1, 2, 1)
    hold on
    plot([1 1e9], [1 1e9], 'k-')
    plot(true_peak_values, best_peak_values, 'x', 'LineWidth', 2)
    ax = gca;
    ax.XScale = 'log';
    ax.YScale = 'log';
    ax.FontSize = 16;
    ax.LineWidth = 2;
    xlabel('True R values (Ohms)')
    ylabel('Best fitted R values (Ohms)')
    title('Transversal resistors')
    xlim([2e3 1e5])
    ylim([2e3 1e5])
    legend('Ideal', 'Model Output', 'Location','southeast')

    subplot(1, 2, 2)
    hold on
    plot([1 1e9], [1 1e9], 'k-')
    plot(true_off_peak_values, best_off_peak_values, 'x', 'LineWidth', 2)
    ax = gca;
    ax.XScale = 'log';
    ax.YScale = 'log';
    ax.FontSize = 16;
    ax.LineWidth = 2;
    xlabel('True R values (Ohms)')
    ylabel('Best fitted R values (Ohms)')
    title('Longitudinal resistors')
    xlim([1e2 1e6])
    ylim([1e2 1e6])
    legend('Ideal', 'Model Output', 'Location','southeast')

    set(gcf, 'PaperUnits', 'centimeters')
    set(gcf, 'PaperSize', [25 10])
    set(gcf, 'PaperPosition', [0 0 25 10])
    saveas(gcf, [output_folder filesep 'fullTest_R_ohms.pdf'])

end

%% Peak values SumSquareError to true value
% in octave across all values

tol = 0.01;
expected_value = 0;
value_all = (log10(best_Zn_ohms) - log10(true_EFI_ohms)).^2;
value = sum(value_all(1:17:end));
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)


%% Off Peak values SumSquareError to true value
% in octave across all values

tol = 0.001;
expected_value = 0;
value_all = (log10(best_Zn_ohms) - log10(true_EFI_ohms)).^2;
value_all(1:17:end) = 0;
value = sum(value_all(:));
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)

%% Transversal Resistors, Median SquarError to true value

tol = 0.01;
expected_value = 0;
value_all = (log10(best_R_ohms) - log10(true_R_ohms)).^2;
value = median(value_all(1:17:end));
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)

%% Transversal Resistors, Max SquarError to true value

tol = 0.05;
expected_value = 0;
value_all = (log10(best_R_ohms) - log10(true_R_ohms)).^2;
value = max(value_all(1:17:end));
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)

%% Longitudinal Resistors, Median SquareError to true value
% in octave across all values

tol = 0.01;
expected_value = 0;
value_all = (log10(best_R_ohms) - log10(true_R_ohms)).^2;
value_all(1:17:end) = NaN;
value = median(value_all(:), 'omitnan');
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)

%% Longitudinal Resistors, Maximum SquareError to true value
% in octave across all values

tol = 0.15;
expected_value = 0;
value_all = (log10(best_R_ohms) - log10(true_R_ohms)).^2;
value_all(1:17:end) = NaN;
value = max(value_all(:));
assert(abs(value - expected_value) <= tol,...
    'Expected value of %d. Measured %d', ...
    expected_value, value)