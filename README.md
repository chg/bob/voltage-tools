# CI Voltage Tools

This repository intends to collect code related to voltage recordings (EFI/TIM/SCINSEV) in Cochlear Implant (CI) users.
It includes at the moment:

- `LadderNetworkModel`: a replication of the Ladder Network model (the 'tissue impedance network') by [Vanpoucke et al. (2004)](https://doi.org/10.1109/TBME.2004.836518)

Current version is v0.2.2, see [changelog](CHANGELOG.md) for latest changes.

Any issue or question can be raised [here](https://gitlab.developers.cam.ac.uk/chg/bob/voltage-tools/-/issues).


## How do I get set up? ##

1. Clone this repository to your computer: (or download [latest version v0.2.2](https://gitlab.developers.cam.ac.uk/chg/bob/voltage-tools/-/archive/v0.2.2/voltage-tools-v0.2.2.zip))

	SSH:
	
	`git clone git@gitlab.developers.cam.ac.uk:chg/bob/voltage-tools.git`
	
	HTTPS:
	
	`git clone https://gitlab.developers.cam.ac.uk/chg/chg/bob/voltage-tools.git`

1. Run `startupVoltage.m`, which is in the `voltage-tools` folder just created.
1. Check that it is set up correctly by running `runtests('dualRunTest')`. This fits the ladder network model twice with the same start values, and should give the same output. Test takes a couple minutes. If you ran the test already, call `clearAllTests` first.

## Tests available

Because outputs may vary across computers/Matlab versions, there are a few tests available to check that the output is sensible.
If you ran tests already and want to re-run them, not just show the output again, call `clearAllTests` first.

You can either run all the tests by calling `runAllTests`, or running one specifically by calling `runtests('mySpecificTest')`.

* `dualRunTest`: This fits the ladder network model twice with the same start values, and should give the same output. Test takes a couple minutes.
* `fullTest`: This fits the ladder network model 200 times to a SCINSEV matrix created from known resistors. If not using any parallel pool, this will take an awfully long time, best running it on a cluster if possible.

## How do I add this repository to an other git repository? ##

* Go to your other git repository:
	
	`cd ..\myExperiment\`


* Add the voltage-tools as a submodule:

	`git submodule add git@gitlab.developers.cam.ac.uk:chg/bob/voltage-tools.git voltage-tools`

* You might want to make sure the voltage-tools is in your path, preferably using a startup file in the top folder of your experiment:

	```Matlab
	% Lines to add in your startup file, in the top folder of your experiment
	[current_path, ~, ~] = fileparts(mfilename('fullpath'));
	addpath(fullfile(current_path, ['voltage-tools']))
	clear current_path
	startupVoltage
	```

* Commit the changes (there should be two new files: `.gitmodules` and the `voltage-tools` repository, who will be seen as one file)

All the files should now be in your local repository. To later update this `voltage-tools` submodule to the latest version, you will need to run:

```bash
cd voltage-tools
git pull origin main
cd ..
git add voltage-tools
git commit -m "Updated voltage-tools to latest version" 
```
