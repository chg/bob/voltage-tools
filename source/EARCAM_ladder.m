classdef EARCAM_ladder < LadderNetworkModel
    % This is for backward compatibility, some old models were saved under
    % the class name of EARCAM_ladder, and it is not be possible to load
    % them with the LadderNetworkModel constructor
end