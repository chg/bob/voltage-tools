classdef LadderNetworkModel < handle & matlab.mixin.CustomDisplay
    % LadderNetworkModel
    %
    % Ladder network based on Vanpoucke et al. 2004.
    %
    % How to use:
    %
    %   ladder_model = LadderNetworkModel; % load model
    %   ladder_model.set_Z(EFI); % load EFI/transimpedance matrix
    %   ladder_model.fit_model(); % Fit model
    %
    %   LadderNetworkModel Properties:
    %       order - oder of the model, default 1
    %       number_fits - number of model fits, default 100
	%       jitter_start_values_percent - start values jitter between fits, default 5
	%       min_max_transversal_resistors_ohms - [min_val_ohms max_val_ohms], default [1e2 1e8]
    %       min_max_longitudinal_resistors_ohms - [min_val_ohms max_val_ohms], default [5e1 1e4]
    %       max_iterations_per_fit_per_parameter - default 200
    %       max_transversal_excursion_octave - default 0.5
    %       max_longitudinal_excursion_octave - default 0.2
    %       max_transversal_current_excursion_percent - default 15
    %       max_excursion_layers_octave - 1
    %       nonlinear_constraints - default 1   
    %       error_scaling - factor by which the SSE is multiplied by. default 1e3
    %
    %   LadderNetworkModel Methods:
    %       fit_model - obj.fit_model() fits the ladder model if an EFI has
    %       been loaded
    %       set_Z - obj.set_Z(Zn) loads transimpedance matrix
    
    properties
        order = 1; % order of the model, default 1
        number_fits = 100; % number of model fits, default 100
        jitter_start_values_percent = 5; % start values jitter between fits, default 5
        min_max_transversal_resistors_ohms = [1e2 1e8]; % [min_val_ohms max_val_ohms], default [1e2 1e8]
        min_max_longitudinal_resistors_ohms = [1e1 1e12]; % [min_val_ohms max_val_ohms], default [5e1 1e4]
        max_iterations_per_fit_per_parameter = 200; % default 200
        max_transversal_excursion_octave = 1; % default 0.5
        max_longitudinal_excursion_octave = 0.2; % default 0.2
        max_transversal_current_excursion_percent = 15; % default 15
        max_excursion_layers_octave = 1; % default 1
        nonlinear_constraints = 1; % default 1
        error_scaling = 1e3; % factor by which the SSE is multiplied by.
    end
    
    % These properties are mainly the results of the modelling
    properties (SetAccess = private)
        Zn_ohms = [];
        n_elec = [];
        modelled_Zn_ohms = [];
        modelled_R_ohms = [];  
        modelled_I_trans = [];
        R_start_values_ohms = [];
        R_min_values_ohms = [];
        R_max_values_ohms = [];
        limits_reached_bool = [];
        status = []; % 0 = no EFI loaded; 1 = EFI loaded; 2 = model fitted
    end
    
    % These properties are mostly for debugging
    properties (Access = private)
        version = '1.1.0';
        x_all_fits = [];
        fval_all_fits = [];
        exit_flag_all_fits = [];
        output_all_fits = [];
    end
    
    % Main methods
    methods
        
        % Constructor function
        function obj = LadderNetworkModel()
            obj.status = 0;
        end
        
        % load method
        function set_Z(obj, user_input_Zn_ohms)
            % set_Z Load the EFI/transimpedance data
            
            % Check if data may be in kOhms
            if max(max(user_input_Zn_ohms)) < 60
                warning('Input values are low, make sure values are in ohms, and not kOhms.')
            end
            
            % Make input symmetrical
            if ~issymmetric(user_input_Zn_ohms)
                warning('Making input symmetric matrix')
                user_input_Zn_ohms = (user_input_Zn_ohms + user_input_Zn_ohms')/2;
            end
            
            if obj.order >= length(user_input_Zn_ohms)
                warning(['Order was too high for the number of contacts, it has been lowered to ' ...
                    num2str(length(user_input_Zn_ohms)-1)])
                obj.order = length(user_input_Zn_ohms)-1;
            end
            
            % Load input Z and init values
            obj.Zn_ohms = user_input_Zn_ohms;
            
            obj.init_model();
            
        end
        
        % model fitting
        function fit_model(obj)
            % fit_model Main fitting function
            
            %% Initialize values for fmincon
            options = optimoptions(...
                'fmincon', ...
                'MaxFunctionEvaluations', ...
                sum((obj.n_elec-obj.order):obj.n_elec)*obj.max_iterations_per_fit_per_parameter);
            
            % To make sure fitting does not stop on a local minimum too early
            options.ConstraintTolerance = options.ConstraintTolerance*1e-5;
            options.StepTolerance = options.StepTolerance*1e-2;
            
            fun = @(x)LadderNetworkModel.minimizing_function(obj.Zn_ohms, obj.n_elec, obj.order, x, obj.error_scaling);
            
            if obj.nonlinear_constraints
                nonlcon = @(x)LadderNetworkModel.limit_diagonal_values(obj.Zn_ohms, obj.n_elec, obj.order, x,...
                    obj.max_longitudinal_excursion_octave, obj.max_transversal_excursion_octave, ...
                    obj.max_transversal_current_excursion_percent, obj.max_excursion_layers_octave);
            else
                nonlcon = [];
            end
            
            % Transform matrices in vectors and log-transform data
            for idx_trial = obj.number_fits:-1:1
                start_values(:, idx_trial) = obj.from_matrix_to_vector(log(obj.R_start_values_ohms(:, :, idx_trial)));
            end
            min_values = obj.from_matrix_to_vector(log(obj.R_min_values_ohms));
            max_values = obj.from_matrix_to_vector(log(obj.R_max_values_ohms));
            
            % Initialize results
            tmp_x_all_fits = zeros(size(start_values, 1), obj.number_fits);
            tmp_fval_all_fits = NaN(1, obj.number_fits);
            tmp_exit_flag_all_fits = NaN(1, obj.number_fits);
            tmp_output_all_fits = cell(1, obj.number_fits);
            
            
            %% Fit Zn for the number of trials
            
            if isempty(gcp('nocreate')) || (obj.number_fits <= 2) % for the dual run test,
                % no parallel pool shall be used to ensure same computing node
                for idx_fitting = 1:obj.number_fits
                    [tmp_x_all_fits(:, idx_fitting), ...
                        tmp_fval_all_fits(idx_fitting),...
                        tmp_exit_flag_all_fits(idx_fitting),...
                        tmp_output_all_fits{idx_fitting}] = ...
                        fmincon(...
                        fun, ...
                        start_values(:, idx_fitting), ...
                        [], [], [], [], ...
                        min_values,... % min
                        max_values, ... % max
                        nonlcon,...
                        options);
                end
            else
                parfor idx_fitting = 1:obj.number_fits
                    [tmp_x_all_fits(:, idx_fitting), ...
                        tmp_fval_all_fits(idx_fitting),...
                        tmp_exit_flag_all_fits(idx_fitting),...
                        tmp_output_all_fits{idx_fitting}] = ...
                        fmincon(...
                        fun, ...
                        start_values(:, idx_fitting), ...
                        [], [], [], [], ...
                        min_values,... % min
                        max_values, ... % max
                        nonlcon,...
                        options);
                end
            end
            
            %% Flag fits that reached the limits
            
            for idx = obj.number_fits:-1:1
                max_value_diff_oct = log10(exp(max_values)./exp(tmp_x_all_fits(:, idx)));
                min_value_diff_oct = log10(exp(tmp_x_all_fits(:, idx))./exp(min_values));
                if any(max_value_diff_oct < 0.1) || any(min_value_diff_oct < 0.1)
                    idx_remove(idx) = 1;
                else
                    idx_remove(idx) = 0;
                end
            end
            
            obj.x_all_fits = tmp_x_all_fits;
            obj.fval_all_fits = tmp_fval_all_fits;
            obj.exit_flag_all_fits = tmp_exit_flag_all_fits;
            obj.output_all_fits = tmp_output_all_fits;
            obj.limits_reached_bool = idx_remove;

            
            %% Transform the data and prepare the output
            for idx_trial = obj.number_fits:-1:1
                obj.modelled_R_ohms(:, :, idx_trial) = ...
                    obj.from_vector_to_matrix(exp(tmp_x_all_fits(:, idx_trial)), obj.n_elec, obj.order);
                obj.modelled_Zn_ohms(:, :, idx_trial) = ...
                    obj.get_node_impedance_matrix(obj.n_elec, obj.order, obj.modelled_R_ohms(:, :, idx_trial));
                obj.modelled_I_trans(:, idx_trial) = ...
                    100*(obj.modelled_Zn_ohms(:, :, idx_trial)*...
                    ones(obj.n_elec, 1)./diag(obj.modelled_R_ohms(:, :, idx_trial)))/obj.n_elec;
            end
            
            obj.status = 2;
            
        end    
    end
    
    % Initialization function
    methods (Access = private)
        function init_model(obj)
            
            if ~(isempty(obj.Zn_ohms))
                
                %% Define n_elec
                obj.n_elec = length(obj.Zn_ohms);               
                
                %% Start and max values on resistors
                Zn_no_diag = obj.remove_diagonal(obj.Zn_ohms);
                transversal_resistors_start_values = round(sum(Zn_no_diag, 2, 'omitnan'));
                longitudinal_resistors_start_values = mean(transversal_resistors_start_values)/10;
                
                default_matrix_resistors = zeros(obj.n_elec);
                obj.R_min_values_ohms = zeros(obj.n_elec);
                obj.R_max_values_ohms = zeros(obj.n_elec);
                
                default_matrix_resistors(eye(obj.n_elec) == 0) = longitudinal_resistors_start_values;
                obj.R_min_values_ohms(eye(obj.n_elec) == 0) = obj.min_max_longitudinal_resistors_ohms(1);
                obj.R_max_values_ohms(eye(obj.n_elec) == 0) = obj.min_max_longitudinal_resistors_ohms(2);
                
                default_matrix_resistors(eye(obj.n_elec) == 1) = transversal_resistors_start_values;
                obj.R_min_values_ohms(eye(obj.n_elec) == 1) = obj.min_max_transversal_resistors_ohms(1);
                obj.R_max_values_ohms(eye(obj.n_elec) == 1) = obj.min_max_transversal_resistors_ohms(2);
                
                values_to_be_removed = triu(tril(ones(obj.n_elec), obj.order), -obj.order) == 0;
                default_matrix_resistors(values_to_be_removed) = NaN;
                obj.R_min_values_ohms(values_to_be_removed) = NaN;
                obj.R_max_values_ohms(values_to_be_removed) = NaN;
                
                obj.R_start_values_ohms = repmat(default_matrix_resistors, ...
                    1, 1, obj.number_fits);
                
                %% Add randomness
                a = 1+obj.jitter_start_values_percent/100;
                b = 1-obj.jitter_start_values_percent/100;
                random_values = (b-a).*rand(obj.n_elec, obj.n_elec, obj.number_fits) + a;
                obj.R_start_values_ohms = obj.R_start_values_ohms.*random_values;
                
                %% Clean up previous fits
                obj.modelled_Zn_ohms = [];
                obj.modelled_R_ohms = [];
                obj.modelled_I_trans = [];
                obj.x_all_fits = [];
                obj.fval_all_fits = [];
                obj.exit_flag_all_fits = [];
                obj.output_all_fits = [];
                obj.limits_reached_bool = [];
                obj.limits_reached_bool = [];
                
                %% Update status
                obj.status = 1;
            end
            
        end   
    end
    
    methods (Hidden)
        function s = saveobj(obj)
            % To avoid glitches when saving to .mat file,
            % properties are saved (saveobj) in a structure and the object is
            % reconstructed when loading it.

            s.order = obj.order;
            
            s.number_fits = obj.number_fits;
            s.jitter_start_values_percent = obj.jitter_start_values_percent;
            s.min_max_transversal_resistors_ohms = obj.min_max_transversal_resistors_ohms;
            s.min_max_longitudinal_resistors_ohms = obj.min_max_longitudinal_resistors_ohms;
            s.max_iterations_per_fit_per_parameter = obj.max_iterations_per_fit_per_parameter;
            s.max_transversal_excursion_octave = obj.max_transversal_excursion_octave;
            s.max_transversal_current_excursion_percent = obj.max_transversal_current_excursion_percent;            
            s.max_longitudinal_excursion_octave = obj.max_longitudinal_excursion_octave;
            s.max_excursion_layers_octave = obj.max_excursion_layers_octave;
            s.nonlinear_constraints = obj.nonlinear_constraints;
            s.error_scaling = obj.error_scaling;
            
            s.Zn_ohms = obj.Zn_ohms;
            s.n_elec = obj.n_elec;
            s.modelled_Zn_ohms = obj.modelled_Zn_ohms;
            s.modelled_R_ohms = obj.modelled_R_ohms;
            s.modelled_I_trans = obj.modelled_I_trans;
            s.R_start_values_ohms = obj.R_start_values_ohms;
            s.R_min_values_ohms = obj.R_min_values_ohms;
            s.R_max_values_ohms = obj.R_max_values_ohms;

            
            s.version = obj.version;
            s.x_all_fits = obj.x_all_fits;
            s.fval_all_fits = obj.fval_all_fits;
            s.exit_flag_all_fits = obj.exit_flag_all_fits;
            s.output_all_fits = obj.output_all_fits;
            s.status = obj.status;
            s.limits_reached_bool = obj.limits_reached_bool;
            
        end

    end
    
    % Functions used in model fitting
    methods (Static, Hidden)       
        function out = minimizing_function(recorded_Zn_ohms, n_elec, order, x, scaling_error)
            
            R = LadderNetworkModel.from_vector_to_matrix(exp(x), n_elec, order);
            
            simulated_Zn = LadderNetworkModel.get_node_impedance_matrix(n_elec, order, R);
            
            % Remove the diagonal
            recorded_Zn_ohms(eye(n_elec) == 1) = NaN;
            simulated_Zn(eye(n_elec) == 1) = NaN;
            
            % Add weighting if wanted
            weights = scaling_error*ones(n_elec);
            
            % calculated Sum-of-Squares error
            out =  sum(sum(((weights.*(log10(simulated_Zn) - log10(recorded_Zn_ohms))).^2), 'omitnan'), 'omitnan');
            
        end
        
        function [c, ceq] = limit_diagonal_values(recorded_Zn_ohms, n_elec, order, x,...
                max_long_excursion, max_trans_excursion, max_trans_current_excursion, ...
                max_excursion_layers_octave)
            % nonlinear constraints for minimizing function
            %
            % makes sure that duiagonal values of the fitted Zn are under or equal to
            % the diagonal values in the original Zn
            % 
            % also limits to the amount of excursion between neighbouring resistors
            
            c = [];
            
            R = LadderNetworkModel.from_vector_to_matrix(exp(x), n_elec, order);
            
            simulatedZn = LadderNetworkModel.get_node_impedance_matrix(n_elec, order, R);
            
            diag_rec = diag(recorded_Zn_ohms);
            diag_simulated = diag(simulatedZn);
            
            diff_diag = diag_simulated-diag_rec;
            diff_positive = max(zeros(size(diff_diag)), diff_diag);
            
            ceq_diagonal = sum(diff_positive.^2);
            
            % limit the variability of resistors            
            ceq_resistors = 0;
            scale_factor = 1; % to make sure there's a large penalty above excursion
            for idx = 0:min(order, n_elec-2)
                if idx == 0
                    diff_diag_long = max(abs(diff(log10(diag(R, idx)))) - max_trans_excursion, 0);
                else
                    diff_diag_long = max(abs(diff(log10(diag(R, idx)))) - max_long_excursion, 0);
                end
                ceq_resistors = ceq_resistors + sum(scale_factor*diff_diag_long.^2);
            end
            
            % Limit the variability of resistors across layers
            ceq_resistors_layers = 0;
            if order > 1
                for idx = 1:n_elec-2
                    diff_layer = max(abs(diff(log10(R(idx, ...
                        (idx+1):min(idx+order, n_elec))))) - max_excursion_layers_octave, 0);
                end
                ceq_resistors_layers = ceq_resistors_layers + sum(scale_factor*diff_layer.^2);
            end
            
            % limit the variability of transversal current
            transv_current = 100*(simulatedZn*ones(n_elec, 1)./diag(R))/n_elec;
            diff_current = max(abs(diff(transv_current)) - max_trans_current_excursion, 0);
            ceq_current = sum(diff_current.^2);
            
            ceq = ceq_diagonal+ceq_resistors+ceq_current+ceq_resistors_layers;
            
        end
        
        function vector_out = from_matrix_to_vector(matrix_in)
            
            vector_out = [];
            for idx = 0:size(matrix_in, 2)-1
                vector_out = [vector_out; diag(matrix_in, idx)];
            end
            vector_out(isnan(vector_out)) = [];
            
        end
        
        function matrix_out = from_vector_to_matrix(vector_in, n_elec, order)
            
            matrix_out = zeros(n_elec);
            
            for idx = 0:order
                idx_vector = 1:(n_elec - idx);
                matrix_out = [matrix_out + diag(vector_in(idx_vector), idx)];
                if idx >= 1
                    matrix_out = [matrix_out + diag(vector_in(idx_vector), -idx)];
                end
                vector_in(idx_vector) = [];
            end
            
            matrix_out(matrix_out == 0) = NaN;
            
        end   
        
    end
    
    methods (Static, Hidden)
        
        function obj = loadobj(s)
            % To avoid glitches when saving to .mat file,
            % properties are saved (saveobj) in a structure and the object is
            % reconstructed when loading it.
            if isstruct(s)
                newObj = LadderNetworkModel();
                fprintf('Loading LadderNetworkModel created with version %s (current version is %s).\n',...
                    s.version, newObj.version)
                
                newObj.order = s.order;
                newObj.number_fits = s.number_fits;
                newObj.jitter_start_values_percent = s.jitter_start_values_percent;
                newObj.min_max_transversal_resistors_ohms = s.min_max_transversal_resistors_ohms;
                newObj.min_max_longitudinal_resistors_ohms = s.min_max_longitudinal_resistors_ohms;
                newObj.max_iterations_per_fit_per_parameter = s.max_iterations_per_fit_per_parameter;
                newObj.max_transversal_excursion_octave = s.max_transversal_excursion_octave;
                newObj.max_longitudinal_excursion_octave = s.max_longitudinal_excursion_octave;
                newObj.nonlinear_constraints = s.nonlinear_constraints;

                switch s.version
                    case '1.1.0'
                        newObj.error_scaling = s.error_scaling;
                        if isfield(s, 'max_transversal_current_excursion_percent')
                            newObj.max_transversal_current_excursion_percent = s.max_transversal_current_excursion_percent;
                        else
                            warning('Default max_transversal_current_excursion_percent loaded.')
                        end
                        if isfield(s, 'max_excursion_layers_octave')
                            newObj.max_excursion_layers_octave = s.max_excursion_layers_octave;
                        else
                            warning('Default max_excursion_layers_octave loaded.')
                        end                        
                    otherwise
                        warning('No error scaling defined in that version of the ladder.')
                end

                newObj.Zn_ohms = s.Zn_ohms;
                newObj.n_elec = s.n_elec;
                newObj.modelled_Zn_ohms = s.modelled_Zn_ohms;
                newObj.modelled_R_ohms = s.modelled_R_ohms;
                newObj.modelled_I_trans = s.modelled_I_trans;
                newObj.R_start_values_ohms = s.R_start_values_ohms;
                newObj.R_min_values_ohms = s.R_min_values_ohms;
                newObj.R_max_values_ohms = s.R_max_values_ohms;
                
                

                
                newObj.x_all_fits = s.x_all_fits;
                newObj.fval_all_fits = s.fval_all_fits;
                newObj.exit_flag_all_fits = s.exit_flag_all_fits;
                newObj.output_all_fits = s.output_all_fits;
                newObj.status = s.status;   
                newObj.limits_reached_bool = s.limits_reached_bool;

                obj = newObj;
            else
                obj = s;
            end
        end

    end
        
    % Utilities
    methods (Static)
        function output_matrix = remove_diagonal(input_matrix)
            % remove_diagonal Replaces diagonal with NaNs
            
            output_matrix = input_matrix;
            for idx = 1:length(input_matrix)
                output_matrix(idx, idx) = NaN;
            end
        end
        
        function Zn = get_node_impedance_matrix(n_elec, order, R)
            % get_node_impedance_matrix calculates Zn based on resistor
            % values
            
            B = sum(n_elec-order:n_elec-1);
            
            A = zeros(n_elec, B);
            Yb = zeros(B, B);
            
            RT = diag(R);
            
            RL = NaN(order, n_elec-1);
            
            for idx = 1:order
                RL(idx, 1:(n_elec-idx)) = diag(R, idx);
            end
            
            
            for k = 1:n_elec
                A(k, k) = 1;
                Yb(k, k) = 1/RT(k);
            end
            
            for idx_order = 1:order
                for k = 1:n_elec-idx_order
                    idx_end = sum(n_elec-idx_order:n_elec);
                    idx_start = idx_end - (n_elec - idx_order);
                    A(k, idx_start + k) = 1;
                    A(k+idx_order, idx_start + k) = -1;
                    Yb(idx_start+k, idx_start+k) = 1/RL(idx_order, k);
                end
            end
            
            Zn = (A*Yb*A')^-1;
            
        end
        
        function [out_Zn, new_elecs] = interpolate_Zn(original_Zn, new_n_elec)
            % Interpolates original EFI with a new number of electrodes.
            %
            % The spread of current for the first and last electrode is the same.
            %
            % If the new stimulating electrode is in-between two of the original
            % electrodes (e.g. 2.3 is between 2 and 3), the new EFI centered on 2.3 is
            % the average of 30% of that given by electrode 2, and 70% of that given by
            % electrode 3 (with the peaks aligned).
            %
            % Average is done on a log scale, since EFI are approximately decaying
            % exponentials.
            %
            % Example:
            %
            %       % Whatever the number of electrodes in the original EFI, tne new
            %       one will have 14
            %       [out_EFI, new_elecs] = interpolate_EFI(original_EFI, 14)
            %
            %
            
            % Log-transform
            original_Zn = log10(original_Zn);
            
            % Get the number of electrodes in previous EFI
            old_n_elec = size(original_Zn, 1);
            
            % Calculate the position of the new electrodes
            new_elecs = linspace(1, old_n_elec, new_n_elec);
            
            % Initialize new EFI
            out_Zn = zeros(new_n_elec);
            
            % For each stimulating electrode
            for idx_i = 1:new_n_elec
                
                % Calculate nearest two stimulating electrodes in the original EFI.
                lower_idx_i = floor(new_elecs(idx_i));
                upper_idx_i = ceil(new_elecs(idx_i));
                
                % Get the corresponding spread (i.e. one line in the orignal EFI)
                % Note that these two lines are shifted to have the peak centred at the
                % same location.
                EFI_lower = original_Zn(lower_idx_i, 1:old_n_elec-1);
                EFI_upper = original_Zn(upper_idx_i, 2:old_n_elec);
                
                % Add them with the proportion of distance to original electrodes
                lower_percentage = 1 - (new_elecs(idx_i)-lower_idx_i);
                upper_percentage = 1 - lower_percentage;
                new_EFI = lower_percentage*EFI_lower + upper_percentage*EFI_upper;
                
                % The end values are calculated similarly, by taking the proportion of
                % the end values of the two neighbouring electrodes
                if upper_percentage ~= 0
                    % Final EFI line
                    new_EFI = [lower_percentage*original_Zn(lower_idx_i, 1)+...
                        upper_percentage*original_Zn(upper_idx_i, 1), ...
                        new_EFI, lower_percentage*original_Zn(lower_idx_i, old_n_elec)+...
                        upper_percentage*original_Zn(upper_idx_i, old_n_elec)];
                    % Final corresponding x values
                    x_new_EFI = [1, (1:old_n_elec-1) + upper_percentage, 16];
                else % in case new electrode is same as original electrode
                    new_EFI = [new_EFI, EFI_upper(end)];
                    x_new_EFI = 1:old_n_elec;
                end
                
                % Interpolate the EFI values at the new electrode locations
                out_Zn(idx_i, :) = interp1(x_new_EFI, new_EFI, new_elecs);
            end
            
            % Transfofm values back
            out_Zn = 10.^(out_Zn);
        end
        
        function [out_Zn] = interpolate_missing_values(original_Zn)
            
            % Log-transform
            original_Zn = log10(original_Zn);
            
            n_elec = size(original_Zn, 1);
            
            % Initialize new EFI
            out_Zn = original_Zn;
            
            
            % For each diagonal
            for idx_i = -(n_elec-1):(n_elec-1)
                
%                 idx_i
                
                % Calculate new diagonal
                [new_diag, ~] = fillmissing(diag(original_Zn, idx_i), 'linear','SamplePoints',1:(n_elec-abs(idx_i)));
                
                idx_diag = diag(ones(1, n_elec-(abs(idx_i))), idx_i);
                
                out_Zn(idx_diag == 1) = new_diag;
            end
            
            % Transfofm values back
            out_Zn = 10.^(out_Zn);
        end        
    end
    
    % Plotting function
    methods
        function plot(obj)
            
            figure
            subplot(2, 2, 1)
            hold on
            plot_Z(obj);
            title('Recorded')
            set(gca, 'FontSize', 16)
            ylim([500 5000])        
            set(gca, 'YTick', [1000 2000])
            
            subplot(2, 2, 2)
            hold on
            plot_Zn(obj);
            ylim([500 5000])
            title('Modelled')
            set(gca, 'FontSize', 16)
            set(gca, 'YTick', [1000 2000])            
            
            subplot(223)
            hold on
            plot_I(obj, 1);
            set(gca, 'FontSize', 16)
            title('Transversal Current')
            
            subplot(224)
            hold on
            hTrans = plot_R(obj, 0, 1);
            hLong = plot_R(obj, 1, 1);
            hTissue = plot_V(obj, 0, 1);
            xlabel('Start node')
            ylabel('Resistance value (Ohms)')
            ylim([1e2 1e5])
            set(gca, 'FontSize', 16)
            legend([hTrans hLong hTissue], {'R_{TRANS}','R_{LONG}','R_{TISSUE}'},...
                'location', 'south', 'Box', 'Off', 'FontSize', 12)
            title('Resistor values')
            
        end
        
        function varargout = plot_I(obj, varargin)
            % Plot transversal current
            %
            % obj.plot_I();
            % [h] = plot_I(obj, error_bool);
            % [h, prctiles_I] = plot_I(obj, error_bool);
            
            if length(varargin) == 1
                error_bool = varargin{1};
            else
                error_bool = 0;
            end
            
            valid_I = obj.modelled_I_trans(:, obj.limits_reached_bool == 0);
            prctiles_I = prctile(valid_I, [25 50 75], 2);
            
            if error_bool
                varargout{1} = errorbar(1:obj.n_elec, prctiles_I(:, 2), ...
                    prctiles_I(:, 2)-prctiles_I(:, 1), prctiles_I(:, 3)-prctiles_I(:, 2), ...
                    'CapSize', 0, 'LineWidth', 2);
            else
                varargout{1} = plot(1:obj.n_elec, prctiles_I(:, 2));
            end
            xlim([0 obj.n_elec+1])
            ylim([0 40])
            ylabel('Transv. current (%)')
            xlabel('Transv. resistor')
            
            varargout{2} = prctiles_I;
        end
        
        function varargout = plot_R(obj, varargin)
            % Plot voltage
            %
            % obj.plot_R();
            % [h] = plot_R(obj, distance_from_peak, error_bool);
            % [h, prctiles_R] = plot_R(obj, distance_from_peak, error_bool);
            
            if length(varargin) == 1
                diag_idx = varargin{1};
                error_bool = 0;
            elseif length(varargin) == 2
                diag_idx = varargin{1};
                error_bool = varargin{2};
            else
                diag_idx = 0;
                error_bool = 0;
            end
            
            for idx = obj.number_fits:-1:1
                all_R(:, idx) = diag(squeeze(obj.modelled_R_ohms(:, :, idx)), diag_idx);
            end
            valid_R = all_R(:, obj.limits_reached_bool == 0);
            
            prctiles_R = prctile(valid_R, [25 50 75], 2);
            
            if error_bool == 1
                varargout{1} = errorbar((1:size(valid_R, 1)), prctiles_R(:, 2), ...
                    prctiles_R(:, 2)-prctiles_R(:, 1), prctiles_R(:, 3)-prctiles_R(:, 2), ...
                    'CapSize', 0, 'LineWidth', 2);
            elseif error_bool == 2
                stretch_values = (0:size(valid_R, 1)-1)*(obj.n_elec-1)/(size(valid_R, 1)-1)+1;
                varargout{1} = plot(stretch_values, prctiles_R(:, 2), 'LineWidth', 2);
            else
                varargout{1} = plot(1:size(valid_R, 1), prctiles_R(:, 2), 'LineWidth', 2);
            end
            xlim([0 obj.n_elec+1])
            set(gca, 'yscale', 'log')
            ylabel('Resistor value (Ohms)')
            xlabel('Start node')
            
            varargout{2} = prctiles_R;
        end        
        
        function varargout = plot_V(obj, varargin)
            % Plot voltage
            %
            % obj.plot_V();
            % [h] = plot_V(obj, distance_from_peak, error_bool);
            % [h, prctiles_V] = plot_V(obj, distance_from_peak, error_bool);
            
            if length(varargin) == 1
                diag_idx = varargin{1};
                error_bool = 0;
            elseif length(varargin) == 2
                diag_idx = varargin{1};
                error_bool = varargin{2};
            else
                diag_idx = 0;
                error_bool = 0;
            end
            
            for idx = obj.number_fits:-1:1
                all_V(:, idx) = diag(squeeze(obj.modelled_Zn_ohms(:, :, idx)), diag_idx);
            end
            valid_V = all_V(:, obj.limits_reached_bool == 0);
            
            prctiles_V = prctile(valid_V, [25 50 75], 2);
            
            if error_bool
                varargout{1} = errorbar(1:size(valid_V, 1), prctiles_V(:, 2), ...
                    prctiles_V(:, 2)-prctiles_V(:, 1), prctiles_V(:, 3)-prctiles_V(:, 2), ...
                    'CapSize', 0, 'LineWidth', 2);
            else
                varargout{1} = plot(1:size(valid_V, 1), prctiles_V(:, 2), 'LineWidth', 2);
            end
            xlim([0 obj.n_elec+1])
            set(gca, 'yscale', 'log')
            ylabel('Scaled voltage (Ohms)')
            xlabel('Stimulating electrode')
            
            varargout{2} = prctiles_V;
        end                
        
        function varargout = plot_Zn(obj)
            % Plot modelled Zn
            
            varargout{1} = plot(median(obj.modelled_Zn_ohms(:,:,obj.limits_reached_bool == 0), 3), ':');
            hold on
            set(gca,'ColorOrderIndex', 1)
            varargout{2} = plot(obj.remove_diagonal(...
                median(obj.modelled_Zn_ohms(:,:,obj.limits_reached_bool == 0), 3)), '-');
            xlim([0 obj.n_elec+1])
            set(gca, 'yscale', 'log')
            ylabel('Scaled voltage (Ohms)')
            xlabel('Recording electrode')
            
        end          
        
        function varargout = plot_Z(obj)
            % Plot input Zn
            
            varargout{1} = plot(obj.remove_diagonal(obj.Zn_ohms));
            xlim([0 obj.n_elec+1])
            set(gca, 'yscale', 'log')
            ylabel('Scaled voltage (Ohms)')
            xlabel('Recording electrode')
            
        end          
        
        
    end
    
    % Display functions
    methods (Access = protected)  
        function header = getHeader(obj)
            if ~isscalar(obj)
                header = getHeader@matlab.mixin.CustomDisplay(obj);
            else
                headerStr = matlab.mixin.CustomDisplay.getClassNameForHeader(obj);
                if obj.status == 0
                    headerStr = [headerStr, ' v' obj.version ' with no node impedance matrix set up.'];
                elseif obj.status >= 1
                    headerStr = [headerStr, ' v' obj.version ' with a ' num2str(obj.n_elec) ...
                        '-contact node impedance matrix loaded'];
                    if obj.status == 1
                        headerStr = [headerStr, ' but not yet fitted.'];
                    elseif obj.status == 2
                        headerStr = [headerStr, ' and fitted.'];
                    end
                end
                
                header = sprintf('%s\n', headerStr);
                
            end
            
        end    
    end
    
    % Set methods
    methods
        function set.order(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', 'integer', '>=', 1};
            prop_name = 'order';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end
        
        function set.number_fits(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', 'integer', '>=', 1};
            prop_name = 'number_fits';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end   
        
        function set.max_transversal_excursion_octave(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', '>', 0};
            prop_name = 'max_transversal_excursion_octave';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end           
        
        function set.max_transversal_current_excursion_percent(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', '>', 0};
            prop_name = 'max_transversal_current_excursion_percent';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end             
        
        function set.max_longitudinal_excursion_octave(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', '>', 0};
            prop_name = 'max_longitudinal_excursion_octave';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end           
        
        function set.max_excursion_layers_octave(obj, value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', '>', 0};
            prop_name = 'max_excursion_layers_octave';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end
        
        function set.jitter_start_values_percent(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', '>=', 0};
            prop_name = 'jitter_start_values_percent';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end   
        
        function set.error_scaling(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', '>', 0};
            prop_name = 'error_scaling';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end         
                
        function set.min_max_transversal_resistors_ohms(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'row', 'nonempty', 'numel', 2, '>', 0};
            prop_name = 'min_max_transversal_resistors_ohms';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end          
        
        function set.min_max_longitudinal_resistors_ohms(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'row', 'nonempty', 'numel', 2, '>', 0};
            prop_name = 'min_max_longitudinal_resistors_ohms';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end       
        
        function set.max_iterations_per_fit_per_parameter(obj,value)
            
            % Settings
            property_class = {'numeric'};
            property_attributes = {'scalar', 'integer', '>=', 1};
            prop_name = 'max_iterations_per_fit_per_parameter';
            
            % Try initialization
            validateattributes(value, property_class, property_attributes)
            tmp_value = obj.(prop_name);
            try
                obj.(prop_name) = value;
                obj.init_model();
            catch error_msg
                obj.(prop_name) = tmp_value;
                obj.init_model();
                warning('%s could not be changed, and was kept to its previous value.\n\nCf. error message below for more info.', prop_name)
                rethrow(error_msg)
            end
        end   
                
    end
end

