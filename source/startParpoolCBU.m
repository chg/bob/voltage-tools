
%%

NumWorkers = 100;
MemPerCPU = 1;
NbHours = 1;
NodeNumber = 'node-i08'; % e.g. 'node-i08'
NodeNumbersToExclude = '';% e.g. 'node-i10'

%%

P = cbupool;
switch P.Profile
    case 'CBU_Slurm_2017'
        P.AdditionalProperties.AdditionalSubmitArgs = sprintf(...
            '--ntasks=%d --mem-per-cpu=%dG --time=%d:00:00 -w %s --exclude=%s', ...
            NumWorkers, MemPerCPU, NbHours, NodeNumber, NodeNumbersToExclude);
        P.NumWorkers = 100;
        parpool_handle = parpool(P, 100);
    case 'CBU_Slurm_2018'
        P.SubmitArguments = sprintf(...
            '--ntasks=%d --mem-per-cpu=%dG --time=%d:00:00 -w %s --exclude=%s', ...
            NumWorkers, MemPerCPU, NbHours, NodeNumber, NodeNumbersToExclude);
        P.NumWorkers = 100;
        parpool_handle = parpool(P, 100);
end

%%