%% Example to fit a ladder network model to an EFI/SCINSEV recorded in a participant
% with a 16-electrode array (AB24 from the CBU pool of participants)
%
% This might take a very long time (2-3 hours) if you are not using
% parallel computing, because it runs the fitting 200 times, and each fit
% might take 40s - 1 minute
%
% If a parallel pool is active (https://uk.mathworks.com/help/parallel-computing/parpool.html),
% it will automatically use it

clear

%% load data

EFI_ohms = [...
    4387 1106 1017  945  927  910  874  803  803  749  731  696  660  642  624  589
    1106 4494 1106  963  927  874  856  803  767  749  713  696  642  642  624  589
    1017 1106 4708 1052  945  910  856  803  785  731  713  696  660  642  624  589
    944  963 1070 4815 1052  945  874  820  803  767  731  696  660  642  624  589
    926  927  945 1070 4494 1070  945  874  945  785  749  731  749  642  642  589
    890  892  892  945 1070 5457 1070  927  874  803  767  731  713  660  642  606
    855  856  856  874  945 1070 5778 1052  945  856  803  767  749  713  642  624
    820  803  803  838  874  927 1070 4387 1106  945  874  803  767  749  696  642
    784  767  767  803  945  856  945 1159 8025 1213  945  856  874  767  749  642
    748  731  749  767  785  803  856  927 1213 5778 1070  927  856  803  767  696
    730  713  713  731  749  767  803  856  945 1070 5136 1034  945  874  803  731
    695  696  678  696  713  731  767  803  874  910 1017 5457 1106  927  856  767
    677  642  642  642  749  713  731  767  874  874  945 1106 6420 1106  927  803
    641  642  642  642  642  660  713  731  767  803  856  945 1106 6099 1070  856
    606  606  606  624  624  624  642  642  731  749  803  820  910 1070 18297 1195
    570  571  553  571  589  589  606  624  642  642  696  731  767  820 1195 19795];

%% Create ladder network model and set parameters

% Create object and load EFI
LNM = LadderNetworkModel;
LNM.set_Z(EFI_ohms);

% set properties
LNM.order = 15;  % fully connected
LNM.jitter_start_values_percent = 5; % jitter start values for each run to ensure different results
LNM.min_max_transversal_resistors_ohms = [1e2 1e8];
LNM.min_max_longitudinal_resistors_ohms = [1 1e12];
LNM.max_iterations_per_fit_per_parameter = 200; % it won't always converge, so this sets a limit when to stop
LNM.max_transversal_excursion_octave = 1; % max excursion between neighbouring transversal resistors
LNM.max_longitudinal_excursion_octave = 0.2; % max excursion between neighbouring longitudinal resistors
LNM.max_transversal_current_excursion_percent = 20; % max excursion between neighbouring transversal current
LNM.max_excursion_layers_octave = 1; % max excursion between neighbouring longitudinal resistors of different layers
LNM.nonlinear_constraints = 1; % setting this to 0 means no constraints
LNM.error_scaling = 1e6; % this increases the error, if too small, fmincon will converge on unsatisfying solutions
LNM.number_fits = 200; % number of times the fit is repeated

%% Fit the data

tic
LNM.fit_model
toc

%% Plot the data

plot(LNM)

%% find best fit, At the moment, it's a bit unclear what the best way
% to pick the best fit is, so take that with a pinch of salt.
% here it is based on closest to median fitted resistors
% exclude conditions where the resistors values are close to the limits
% imposed, as this usually is a sign of non-convergence
median_R_ohms = median(LNM.modelled_R_ohms(:, :, LNM.limits_reached_bool == 0), 3);
for idx = LNM.number_fits:-1:1
    if ~LNM.limits_reached_bool(idx)
        R_ohms_here = squeeze(LNM.modelled_R_ohms(:, :, idx));
        out_R(idx) =  sum(sum((log10(R_ohms_here) - log10(median_R_ohms)).^2, 'omitnan'), 'omitnan');
    else
        out_R(idx) = NaN;
    end
end
[~, idx_best_R] = min(out_R);
best_R_ohms = squeeze(LNM.modelled_R_ohms(:, :, idx_best_R));
best_Zn_ohms = squeeze(LNM.modelled_Zn_ohms(:, :, idx_best_R));


%% Plot the "best fit"

figure
hold on
EFI_ohms_no_peaks = EFI_ohms;
EFI_ohms_no_peaks(1:17:end) = NaN;
hData = plot(EFI_ohms_no_peaks);
set(gca, 'ColorOrderIndex', 1)
hFit = plot(best_Zn_ohms, 'LineWidth', 2);
xlim([0 17])
ylim([400 2500])
legend([hData(1), hFit(1)], {'measured', 'fitted'})
xlabel('Electrode')
ylabel('Scaled Voltage (Ohms)')

%% Plot resistors for a subset of electrodes

figure
for idx_plot = 1:2
    switch idx_plot
        case 1
            data_R_here = best_R_ohms;
            str_plot = 'Best fit';
        case 2
            data_R_here = median_R_ohms;
            str_plot = 'Median fit';
    end
    
    subplot(1, 2, idx_plot)
    hold on
    subset_EL = [1 6 11 16];
    clear hLong hTrans
    for idx_subset = 1:numel(subset_EL)
        data_here_long = data_R_here(subset_EL(idx_subset), :);
        data_here_trans = data_here_long(subset_EL(idx_subset));
        data_here_long(subset_EL(idx_subset)) = NaN;
        set(gca, 'ColorOrderIndex', idx_subset)
        hLong(idx_subset) = plot(1:16, data_here_long, 'x-', 'LineWidth', 2);
        set(gca, 'ColorOrderIndex', idx_subset)
        hTrans(idx_subset) = plot(20, data_here_trans, 'o', 'LineWidth', 2);

    end
    xlim([0 25])
    xlabel('End node')
    ylabel('resistor value (ohms)')
    ax = gca;
    ax.YScale = 'log';
    ylim([1e3 1e6])
    ax.YTick = [1e3 1e4 1e5 1e6];
    ax.XTick = [subset_EL 20];
    ax.XTickLabel = {num2str(subset_EL'); 'GND'};
    ax.FontSize = 16;
    ax.LineWidth = 2;
    hLeg = legend(hLong, {num2str(subset_EL')});
    hLeg.Title.String = 'Start Node';
    hLeg.Location = 'northeast';
    title(str_plot)
end