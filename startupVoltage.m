% Startup file

[current_path, ~, ~] = fileparts(mfilename('fullpath'));
addpath(fullfile(current_path, 'source'))
addpath(fullfile(current_path, 'tests'))
addpath(genpath(fullfile(current_path, 'database_fitted_networks')))
clear current_path
