# Changelog


## [Unreleased](https://gitlab.developers.cam.ac.uk/chg/bob/voltage-tools/compare?from=v0.2.2&to=dev)

## [v0.2.2](https://gitlab.developers.cam.ac.uk/chg/bob/voltage-tools/compare?from=v0.2.1&to=v0.2.2)

### Added

- Add example for fitting a fully connected LNM
- Add license

## [v0.2.1](https://gitlab.developers.cam.ac.uk/chg/bob/voltage-tools/compare?from=v0.2.0&to=v0.2.1)

### Changed

- Fix typos in readme and changelog

## [v0.2.0](https://gitlab.developers.cam.ac.uk/chg/bob/voltage-tools/compare?from=v0.1.0&to=v0.2.0)

### Changed

- Changed name of ladder class to LadderNetworkModel
- Update readme with references to tests

### Added
- Add source code for ladder network
- Add test that runs fitting twice, fixed start values
- Add full test with known resistor values
- Add code to run on CBU cluster

### Changed

- Changed name of ladder class to LadderNetworkModel
- Update readme with references to tests

### Added
- Add source code for ladder network
- Add test that runs fitting twice, fixed start values
- Add full test with known resistor values
- Add code to run on CBU cluster


## [v0.1.0](https://gitlab.developers.cam.ac.uk/chg/bob/voltage-tools/compare?from=v0.0.0&to=v0.1.0)

### Added

- Add basic info to Readme
